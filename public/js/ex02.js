"use strict";

$(function() {

    $("#form").submit(function(event) {

        var email = $("#email").val();

        if (!(email.endsWith("@auckland.ac.nz") || email.endsWith("@aucklanduni.ac.nz") || email.endsWith("@waikato.ac.nz"))) {
            event.preventDefault();
        }

    });

});